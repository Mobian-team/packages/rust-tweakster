use std::{ path::PathBuf, fs };
use std::os::unix;
use serde::Deserialize;
use merge::Merge;

use crate::utils;

#[derive(Deserialize, Debug, Default, Clone, Merge)]
struct SystemdRule {
    #[merge(skip)]
    service: String,
    #[merge(strategy = merge::vec::append)]
    fragments: Vec<String>,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    system: Option<Vec<SystemdRule>>,
    user: Option<Vec<SystemdRule>>,
    enable: Option<Vec<String>>,
}

fn process_services(rules: Vec<SystemdRule>,
                    origdir: PathBuf,
                    destdir: PathBuf) -> Result<(), std::io::Error> {
    for rule in rules {
        let mut origin = PathBuf::from(&origdir);
        origin.push(&rule.service);
        origin.set_extension("service.d");

        let mut fragments_dir = PathBuf::from(&destdir);
        fragments_dir.push(&rule.service);
        fragments_dir.set_extension("service.d");
        
        fs::create_dir_all(&fragments_dir)?;

        for fragment in &rule.fragments {
            let mut orig = PathBuf::from(&origin);
            orig.push(fragment);

            let mut link = PathBuf::from(&fragments_dir);
            link.push(fragment);
            
            println!("systemd: {:?} -> {:?}", link, orig);

            if fs::read_link(&link).is_err() {
                unix::fs::symlink(orig, link)?;
            }
        }
    }

    Ok(())
}

pub fn process(systemd: Config, root: &PathBuf) -> Result<(), std::io::Error> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/systemd");

    let mut services_dir = PathBuf::from(root);
    services_dir.push("etc/systemd/");
    
    if let Some(rules) = systemd.system {
        let mut orig_dir = PathBuf::from(&settings_dir);
        orig_dir.push("system");

        let mut dest_dir = PathBuf::from(&services_dir);
        dest_dir.push("system");

        process_services(rules, orig_dir, dest_dir)?;
    }

    if let Some(rules) = systemd.user {
        let mut orig_dir = PathBuf::from(&settings_dir);
        orig_dir.push("user");

        let mut dest_dir = PathBuf::from(&services_dir);
        dest_dir.push("user");

        process_services(rules, orig_dir, dest_dir)?;
    }

    if let Some(services) = systemd.enable {
        for service in services {
            println!("systemd: enable {}", service);
            utils::execute("/usr/bin/systemctl",
                           Some(vec!["enable", "--now", service.as_str()]));
        }
    }

    utils::execute("/usr/bin/systemctl", Some(vec!["daemon-reload"]));

    Ok(())
}
