use std::{ path::PathBuf, fs };
use std::io::Write;
use std::os::unix;
use serde::Deserialize;
use merge::Merge;

#[derive(Deserialize, Debug, Default, Clone, Merge)]
struct PulseConfig {
    format: Option<String>,
    #[merge(strategy = merge::vec::append)]
    rates: Vec<u32>,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    config: Option<PulseConfig>,
    script: Option<String>,
}

pub fn process(pulse: Config, root: &PathBuf) -> Result<(), std::io::Error> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks");

    let mut pulse_dir = PathBuf::from("/");
    pulse_dir.push("etc/pulse");

    let mut generated = PathBuf::from(root);
    generated.push("etc/mobile-tweaks/generated");
    fs::create_dir_all(&generated)?;

    generated.push("pulse");
    generated.set_extension("conf");

    if pulse.config.is_none() && pulse.script.is_none() {
        return Ok(());
    }

    let mut file = fs::File::create(&generated)?;

    if let Some(ref script) = pulse.script {
        let mut orig = PathBuf::from(&settings_dir);
        orig.push("pulse");
        orig.push(script);

        let mut link = PathBuf::from(root);
        link.push("etc/pulse");
        fs::create_dir_all(&link)?;
        link.push(script);

        println!("pulse: {:?} -> {:?}", link, orig);

        if fs::read_link(&link).is_err() {
            unix::fs::symlink(orig, link)?;
        }

        writeln!(file, "default-script-file={}/{}", pulse_dir.to_str().unwrap(), script)?;
    }

    if let Some(ref config) = pulse.config {
        if let Some(ref fmt) = config.format {
            writeln!(file, "default-sample-format={}", fmt)?;
        }

        writeln!(file, "default-sample-rate={}", config.rates[0])?;

        if config.rates.len() > 1 {
            writeln!(file, "alternate-sample-rate={}", config.rates[1])?;
        }
    }

    file.sync_all()?;

    let mut orig = PathBuf::from(&settings_dir);
    orig.push("generated");
    orig.push("pulse");
    orig.set_extension("conf");

    let mut link = PathBuf::from(root);
    link.push("etc/pulse");
    link.push("daemon.conf.d");
    fs::create_dir_all(&link)?;
    link.push("99-mobile-tweaks");
    link.set_extension("conf");

    println!("pulse: {:?} -> {:?}", link, orig);

    if fs::read_link(&link).is_err() {
        unix::fs::symlink(orig, link)?;
    }

    Ok(())
}
