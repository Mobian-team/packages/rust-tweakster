use std::{ path::PathBuf, fs };
use std::os::unix;

use crate::utils;

pub fn process(uboot: utils::FilesArray, root: &PathBuf) -> Result<(), std::io::Error> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/u-boot");

    let mut uboot_dir = PathBuf::from(root);
    uboot_dir.push("etc/u-boot-menu.d");
    fs::create_dir_all(&uboot_dir)?;

    for ref name in uboot.files {
        let mut orig = PathBuf::from(&settings_dir);
        orig.push(name);

        let mut link = PathBuf::from(&uboot_dir);
        link.push(name);

        println!("uboot: {:?} -> {:?}", link, orig);

        if fs::read_link(&link).is_err() {
            unix::fs::symlink(orig, link)?;
        }
    }

    utils::execute("/usr/sbin/u-boot-update", None);

    Ok(())
}
