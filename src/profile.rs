use std::{ path::PathBuf, fs };
use std::os::unix;

use crate::utils;

pub fn process(profile: utils::FilesArray, root: &PathBuf) -> Result<(), std::io::Error> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/profile");

    let mut profile_dir = PathBuf::from(root);
    profile_dir.push("etc/profile.d");
    fs::create_dir_all(&profile_dir)?;

    for ref name in profile.files {
        let mut orig = PathBuf::from(&settings_dir);
        orig.push(name);

        let mut link = PathBuf::from(&profile_dir);
        link.push(name);

        println!("profile: {:?} -> {:?}", link, orig);

        if fs::read_link(&link).is_err() {
            unix::fs::symlink(orig, link)?;
        }
    }

    Ok(())
}
