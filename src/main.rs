mod default;
mod gsettings;
mod profile;
mod pulse;
mod systemd;
mod uboot;
mod udev;
mod utils;

use std::{ path::PathBuf, fs };
use structopt::StructOpt;
use serde::Deserialize;
use merge::Merge;

#[derive(Debug, StructOpt)]
#[structopt(about = "Utility for handling device-specific tweaks")]
struct Opt {
    /// Chroot to work into
    #[structopt(short, long)]
    chroot: Option<PathBuf>,

    /// Device type (default: auto-detect)
    #[structopt(short, long)]
    device: Option<String>,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct IncludeArray {
    #[merge(strategy = merge::vec::append)]
    pub include: Vec<String>
}

#[derive(Deserialize, Debug, Default, Clone)]
struct Config {
    general: Option<IncludeArray>,
    default: Option<utils::FilesArray>,
    gsettings: Option<gsettings::Config>,
    profile: Option<utils::FilesArray>,
    pulse: Option<pulse::Config>,
    systemd: Option<systemd::Config>,
    uboot: Option<utils::FilesArray>,
    udev: Option<udev::Config>,
}

fn detect_device(root: &PathBuf) -> Option<String> {
    let mut path = PathBuf::from(root);
    path.push("proc/device-tree/compatible");

    let contents = match fs::read_to_string(path) {
        Ok(str) => str,
        _ => Default::default(),
    };

    let compatibles: Vec<&str> = contents.split("\0").filter(|s| s.len() > 0).collect();

    let mut cfg_path = PathBuf::from(root);
    cfg_path.push("etc/mobile-tweaks/configs");

    for entry in fs::read_dir(cfg_path) {
        for file in entry {
            let fname = match file {
                Ok(dirent) => dirent.file_name(),
                _ => continue,
            };
            for value in compatibles.clone() {
                let full_name = String::from(value) + ".toml";
                if &fname == full_name.as_str() {
                    return Some(value.to_string());
                }
            }
        }
    }

    None
}

fn main() -> Result<(), std::io::Error> {
    let opt = Opt::from_args();

    let root = match opt.chroot {
        Some(path) => path,
        _ => PathBuf::from("/"),
    };

    let device = match opt.device {
        Some(str) => str,
        _ => detect_device(&root).unwrap(),
    };

    let mut cfg_dir = PathBuf::from(&root);
    cfg_dir.push("etc/mobile-tweaks/configs");

    let mut cfg_path = PathBuf::from(&cfg_dir);
    cfg_path.push(&device);
    cfg_path.set_extension("toml");

    let contents = match fs::read_to_string(cfg_path) {
        Ok(str) => str,
        _ => "".to_string(),
    };

    let config: Config = toml::from_str(contents.as_str()).unwrap();

    println!("Device is '{}'", device);

    let mut final_conf: Config = Default::default();

    if let Some(ref general) = config.general {
        for inc in &general.include {
            let mut comps = inc.as_str().split(".");
            let mut cfg_path = PathBuf::from(&cfg_dir);

            cfg_path.push(comps.next().unwrap());
            cfg_path.set_extension("toml");

            let cfg_toml = match fs::read_to_string(cfg_path) {
                Ok(str) => str,
                _ => "".to_string(),
            };

            let cfg: Config = toml::from_str(cfg_toml.as_str()).unwrap();

            match comps.next() {
                Some("default") => final_conf.default = cfg.default,
                Some("gsettings") => final_conf.gsettings = cfg.gsettings,
                Some("profile") => final_conf.profile = cfg.profile,
                Some("pulse") => final_conf.pulse = cfg.pulse,
                Some("systemd") => final_conf.systemd = cfg.systemd,
                Some("uboot") => final_conf.uboot = cfg.uboot,
                Some("udev") => final_conf.udev = cfg.udev,
                Some(s) => println!("Unrecognized section '{}'", s),
                None => println!("Error: no section"),
            }
        }
    }

    if let Some(ref mut default) = final_conf.default {
        default.merge(config.default.unwrap_or_default());
    } else {
        final_conf.default = config.default;
    }
    if let Some(ref mut settings) = final_conf.gsettings {
        settings.merge(config.gsettings.unwrap_or_default());
    } else {
        final_conf.gsettings = config.gsettings;
    }
    if let Some(ref mut profile) = final_conf.profile {
        profile.merge(config.profile.unwrap_or_default());
    } else {
        final_conf.profile = config.profile;
    }
    if let Some(ref mut pulse) = final_conf.pulse {
        pulse.merge(config.pulse.unwrap_or_default());
    } else {
        final_conf.pulse = config.pulse;
    }
    if let Some(ref mut systemd) = final_conf.systemd {
        systemd.merge(config.systemd.unwrap_or_default());
    } else {
        final_conf.systemd = config.systemd;
    }
    if let Some(ref mut uboot) = final_conf.uboot {
        uboot.merge(config.uboot.unwrap_or_default());
    } else {
        final_conf.uboot = config.uboot;
    }
    if let Some(ref mut udev) = final_conf.udev {
        udev.merge(config.udev.unwrap_or_default());
    } else {
        final_conf.udev = config.udev;
    }

    if let Some(default) = final_conf.default {
        default::process(default, &root)?;
    }
    if let Some(settings) = final_conf.gsettings {
        gsettings::process(settings, &root)?;
    }
    if let Some(profile) = final_conf.profile {
        profile::process(profile, &root)?;
    }
    if let Some(pulse) = final_conf.pulse {
        pulse::process(pulse, &root)?;
    }
    if let Some(systemd) = final_conf.systemd {
        systemd::process(systemd, &root)?;
    }
    if let Some(uboot) = final_conf.uboot {
        uboot::process(uboot, &root)?;
    }
    if let Some(udev) = final_conf.udev {
        udev::process(udev, &root)?;
    }

    Ok(())
}
