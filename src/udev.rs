use std::{ path::PathBuf, fs };
use std::os::unix;
use serde::Deserialize;
use merge::Merge;

use crate::utils;

#[derive(Deserialize, Debug, Default, Clone)]
struct UdevRule {
    name: String,
    priority: u32,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    #[merge(strategy = merge::vec::append)]
    rules: Vec<UdevRule>
}

pub fn process(udev: Config, root: &PathBuf) -> Result<(), std::io::Error> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/udev");

    let mut udev_dir = PathBuf::from(root);
    udev_dir.push("etc/udev/rules.d");
    fs::create_dir_all(&udev_dir)?;

    for ref rule in udev.rules {
        let mut orig = PathBuf::from(&settings_dir);
        orig.push(&rule.name);
        orig.set_extension("rules");
        
        let mut link = PathBuf::from(&udev_dir);
        link.push(format!("{}-{}", rule.priority, rule.name));
        link.set_extension("rules");

        println!("udev: {:?} -> {:?}", link, orig);

        if fs::read_link(&link).is_err() {
            unix::fs::symlink(orig, link)?;
        }
    }

    utils::execute("/usr/bin/udevadm", Some(vec!["control", "--reload"]));

    Ok(())
}
