use std::{ path::PathBuf, fs };
use std::os::unix;

use crate::utils;

pub fn process(default: utils::FilesArray, root: &PathBuf) -> Result<(), std::io::Error> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/default");

    let mut default_dir = PathBuf::from(root);
    default_dir.push("etc/default");
    if !default_dir.exists() {
        fs::create_dir_all(&default_dir)?;
    }

    for ref name in default.files {
        let mut orig = PathBuf::from(&settings_dir);
        orig.push(name);

        let mut link = PathBuf::from(&default_dir);
        link.push(name);

        println!("default: {:?} -> {:?}", link, orig);

        if fs::read_link(&link).is_err() {
            unix::fs::symlink(orig, link)?;
        }
    }

    Ok(())
}
