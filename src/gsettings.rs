use std::{path::PathBuf, fs};
use std::io::Write;
use serde::Deserialize;
use merge::Merge;

use crate::utils;

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    #[merge(strategy = merge::vec::append)]
    overrides: Vec<String>
}

pub fn process(settings: Config, root: &PathBuf) -> Result<(), std::io::Error> {
    let mut settings_dir = PathBuf::from(root);
    settings_dir.push("etc/mobile-tweaks");

    let mut generated = PathBuf::from(&settings_dir);
    generated.push("generated");
    fs::create_dir_all(&generated)?;

    generated.push("mobile-tweaks");
    generated.set_extension("gschema.override");

    let mut file = fs::File::create(&generated)?;
    println!("gsettings: creating {:?}", generated);

    settings_dir.push("gsettings");

    for ref name in settings.overrides {
        let mut settings_file = PathBuf::from(&settings_dir);
        settings_file.push(name);
        settings_file.set_extension("gschema.override");

        let mut orig = fs::File::open(&settings_file)?;

        println!("gsettings: appending {:?}", settings_file);

        std::io::copy(&mut orig, &mut file)?;
        file.write(b"\n")?;
    }

    file.sync_all()?;

    utils::execute("/usr/bin/glib-compile-schemas",
                   Some(vec!["/usr/share/glib-2.0/schemas/"]));

    Ok(())
}
